from django.apps import AppConfig


class DrthConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'drth'
