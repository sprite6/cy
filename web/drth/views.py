from django.shortcuts import render
from drth.models import Connect


# Create your views here.


def index(requet):
    # 查询出Connect对象信息，也就是数据表中的所有数据
    # 一行数据就是一个对象，一个格子的数据就是一个对象的一个属性值
    objs = Connect.objects.all()

    # locals函数可以将该函数中出现过的所有变量传入到展示页面中，即index.html文件中
    return render(requet,'index.html',locals())