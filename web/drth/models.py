from django.db import models

# Create your models here.
class Connect(models.Model):
    id = models.CharField(max_length=30,verbose_name='id',primary_key=True)
    rule_name = models.CharField(max_length=30,verbose_name='rule_name')
    url = models.CharField(max_length=255,verbose_name='url')
    content = models.CharField(max_length=30,verbose_name='content')
    enabled = models.IntegerField(verbose_name='enabled')
    del_flag = models.IntegerField(verbose_name='del_log')
    created_time = models.DateTimeField(max_length=6,verbose_name='created_time')
    updated_time = models.DateTimeField(max_length=6,verbose_name='updated_time')
    created_by = models.CharField(max_length=30,verbose_name='created_by')
    updated_by = models.CharField(max_length=30,verbose_name='updated_by')
    version = models.CharField(max_length=20,verbose_name='version')