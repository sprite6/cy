from django.db import models

# Create your models here.


class device(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=32)
    ip = models.CharField(max_length=32, unique=True)
    password = models.CharField(max_length=32)
