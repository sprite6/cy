from django.shortcuts import render
import json
# Create your views here.
from django.http import HttpResponse
from pymysql import IntegrityError

from myapp import models


def index(request):
    return HttpResponse("Hello, world.")


def add_device(request):
    if request.method == 'POST':
        ip = request.POST['ip']
        name = request.POST['name']
        password = request.POST['password']
        try:
            models.device.objects.create(ip=ip, name=name, password=password)
        except:
            return HttpResponse("设备添加失败！")
        else:
            return HttpResponse("设备添加成功！")
    return render(request, "add_device.html")


def add(request):
    if request.method == 'POST':
        postBody = request.body
        json_result = json.loads(postBody)
        print(json_result)
        models.device.objects.create(ip=json_result['ip'], name=json_result['name'], password=json_result['password'])
        return HttpResponse("设备添加成功！")
    else:
        return HttpResponse("方法错误！")
